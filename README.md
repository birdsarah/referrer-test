* Start server1 (see server1/README.md)
* Start ngrok for 8001
* Enter ngrok link in server2/index.html
* Start server2 (see server2/README.md)
